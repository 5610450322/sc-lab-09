package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		
		List<Node> treeTraversal;
		if(node==null){
			return new ArrayList<Node>();
		}
		else{
			treeTraversal = traverse(node.getLeft());
			treeTraversal.add(node);
			treeTraversal.addAll(traverse(node.getRight()));
			return treeTraversal;
		}
	}

}
