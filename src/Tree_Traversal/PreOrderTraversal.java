package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		
		List<Node> treeTraversal = new ArrayList<Node>();
		if(node == null){
			return new ArrayList<Node>();
		}
		else{
			treeTraversal.add(node);
			treeTraversal.addAll(traverse(node.getLeft()));
			treeTraversal.addAll(traverse(node.getRight()));
			return treeTraversal;
		}
	}

}
