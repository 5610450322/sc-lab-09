package Tree_Traversal;

public class Test {
	public static void main(String[] args){
		Node a = new Node("A", null, null);
		Node c = new Node("C", null, null);
		Node e = new Node("E", null, null);
		Node d = new Node("D", c, e);
		Node b = new Node("B", a, d);
		Node h = new Node("H", null, null);
		Node i = new Node("I", h, null);
		Node g = new Node("G", null, i);
		Node f = new Node("F", b, g);
		
		ReportConsole rc = new ReportConsole();
		rc.display(f, new PreOrderTraversal());
		rc.display(f, new InOrderTraversal());
		rc.display(f, new PostOrderTraversal());
		
		
		
	}

}
