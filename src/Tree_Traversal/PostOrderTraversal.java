package Tree_Traversal;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal {

	@Override
	public List<Node> traverse(Node node) {
		
		List<Node> treeTraversal;
		if(node == null){
			return new ArrayList<Node>();
		}
		else{
			treeTraversal = traverse(node.getLeft());
			treeTraversal.addAll(traverse(node.getRight()));
			treeTraversal.add(node);
			return treeTraversal;
		}
	}

}
