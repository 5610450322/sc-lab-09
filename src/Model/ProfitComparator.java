package Model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		double profit1 = o1.getIncome() - o1.getExpenses();
		double profit2 = o2.getIncome() - o2.getExpenses();
		if (profit1 < profit2){
			return -1;
		}
		else if (profit1 > profit2){
			return 1;
		}
		return 0;
	}

}
