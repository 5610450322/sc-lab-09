package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Taxable>{

	@Override
	public int compare(Taxable o1, Taxable o2) {
		double tax1 = o1.getTax();
		double tax2 = o2.getTax();
		if (tax1 < tax2){
			return -1;
		}
		else if (tax1 > tax2){
			return 1;
		}
		return 0;
	}



}
