package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		double expenses1 = o1.getExpenses();
		double expenses2 = o2.getExpenses();
		if (expenses1 < expenses2){
			return -1;
		}
		else if (expenses1 > expenses2){
			return 1;
		}
		return 0;
	}

}
