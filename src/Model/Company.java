package Model;

import Interface.Taxable;

public class Company implements Taxable {
	private String name;
	private double income;
	private double expenses;

	public Company(String name, double income, double expenses) {
		this.name = name;
		this.income = income;
		this.expenses = expenses;
	}

	public String getName() {
		return this.name;
	}

	public double getIncome() {
		return this.income;
	}

	public double getExpenses() {
		return this.expenses;
	}

	@Override
	public double getTax() {
		return 0.3 * (this.getIncome() - this.getExpenses());
	}
	
	public String toString(){
		return "Name : " + getName() + "\t" + "Income : " + getIncome() + "\t" + "Expenses : " + getExpenses() 
				+ "\t" + "Tax : " + getTax();
	}
}
